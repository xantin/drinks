import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DrinkEditorComponent } from './drink-editor/drink-editor.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';

import { EventComponent } from './event/event.component';
import { DrinkStatisticsComponent } from './drink-statistics/drink-statistics.component';
import { DrinksInTimeComponent } from './drinks-in-time/drinks-in-time.component';

import { DrinkListComponent } from './drink-list/drink-list.component';
import { IndicatorsComponent } from './indicators/indicators.component';

import { ChartsModule } from 'ng2-charts';
import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  declarations: [
    AppComponent,
    DrinkEditorComponent,
    DashboardComponent,
    EventComponent,
    DrinkStatisticsComponent,
    DrinksInTimeComponent,
    DrinkListComponent,
    IndicatorsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    ChartsModule,
    NgCircleProgressModule.forRoot({
      radius: 60,
      showUnits: false,
      backgroundColor: 'transparent',
      backgroundStroke: 'transparent',
      outerStrokeWidth: 8,
      innerStrokeWidth: 8,
      outerStrokeColor: '#585858',
      innerStrokeColor: '#2f2f2f',
      animationDuration: 300,
      space: -8,
      outerStrokeLinecap: 'square',
      showSubtitle: false,
      titleFontSize: '30',
      titleColor: '#fff',
      responsive: true,
    })
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

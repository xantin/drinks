import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinksInTimeComponent } from './drinks-in-time.component';

describe('DrinksInTimeComponent', () => {
  let component: DrinksInTimeComponent;
  let fixture: ComponentFixture<DrinksInTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrinksInTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinksInTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drinks-in-time',
  templateUrl: './drinks-in-time.component.html',
  styleUrls: ['./drinks-in-time.component.css']
})
export class DrinksInTimeComponent implements OnInit {
  // https://pusher.com/tutorials/data-visualization-angular

  // lineChart
  public lineChartData: Array<any> = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
    { data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C' },
    { data: [10, 48, 77, 9, 70, 27, 80], label: 'Series C' },
    { data: [2, 4, 7, 9, 6, 2, 4], label: 'Series C' },
  ];
  public lineChartLabels: Array<any> = ['-3.5', '-3', '-2.5', '-2', '-1.5', '-1', '-0.5'];
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          display: false,
          beginAtZero: true,
      },
      gridLines : {
        display: false,
        color: '#787878',
      },
      }],
      xAxes: [{
        display: true,
        gridLines : {
          display: false,
          color: '#787878',
        },
      }]
    }
  };
  colors = ['#d0021b', '#f5a623', '#f4e31c', '#7ed321', '#4582cb'];
  options = {
    fill: false,
    pointRadius: 0
  };
  lineChartColors = [];
  lineChartLegend = false;
  lineChartType = 'line';

  constructor() {
    this.lineChartColors = this.colors.map((c) => {
      return {
        borderColor: c,
        ...this.options,
      };
    });
  }

  ngOnInit() {
  }

}

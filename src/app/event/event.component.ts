import { Component, OnInit } from '@angular/core';
import { DrinkService } from '../drink.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  data;

  constructor(private drinkService: DrinkService) { }

  async ngOnInit() {
    this.data = await this.drinkService.getEvents();
  }

}

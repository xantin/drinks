import { Component, OnInit, AfterContentInit } from '@angular/core';
import { DrinkService } from '../drink.service';

@Component({
  selector: 'app-drink-statistics',
  templateUrl: './drink-statistics.component.html',
  styleUrls: ['./drink-statistics.component.css']
})
export class DrinkStatisticsComponent implements OnInit {

  datasets;
  chartColors;
  labels;
  chartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          display: false,
          beginAtZero: true,
        },
        gridLines: {
          display: false,
          color: '#787878',
        },
        display: false,
      }],
      xAxes: [{
        barPercentage: 0.2,
        display: true,
        gridLines: {
          display: false,
          color: '#787878',
        },
      }]
    }
  };
  legend = false;
  type = 'bar';
  constructor(private drinkService: DrinkService) {
  }

  async ngOnInit() {
    const datasets = await this.drinkService.getDrinks();
    this.datasets = [{ data: datasets.map((d) => d.value), label: 'dd' }];

    const colors = datasets.map((c) => c.color);
    this.chartColors = [{
      borderColor: colors,
      backgroundColor: colors,
    }];
    this.labels = datasets.map((i) => i.value);
  }
}

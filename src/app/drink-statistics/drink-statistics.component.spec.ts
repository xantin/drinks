import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrinkStatisticsComponent } from './drink-statistics.component';

describe('DrinkStatisticsComponent', () => {
  let component: DrinkStatisticsComponent;
  let fixture: ComponentFixture<DrinkStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrinkStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinkStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

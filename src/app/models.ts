export interface Drink {
    id: number | string;
    name: string;
    volume: string;
    price: number;
}

export interface DrinkEvent {
    date: string;
    name: string;
}

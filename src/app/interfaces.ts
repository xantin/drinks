export interface Event {
    time: string;
    id: string | number;
}
export interface Stat {
    percent: number;
    value: number;
    title: string;
    subtitle: string;
}
export interface Drink {
    value: number;
    id: number;
    name: string;
    color: string;
    likes: number;
}

import { Component, OnInit } from '@angular/core';
import { DrinkService } from '../drink.service';

@Component({
  selector: 'app-indicators',
  templateUrl: './indicators.component.html',
  styleUrls: ['./indicators.component.css']
})
export class IndicatorsComponent implements OnInit {
  stats;
  constructor(private drinkService: DrinkService) { }
  async ngOnInit() {
    this.stats = await this.drinkService.getStats();
  }
}

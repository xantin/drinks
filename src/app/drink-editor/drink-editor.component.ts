import { Component, OnInit } from '@angular/core';
import { Drink, DrinkEvent } from '../models';

@Component({
  selector: 'app-drink-editor',
  templateUrl: './drink-editor.component.html',
  styleUrls: ['./drink-editor.component.css']
})
export class DrinkEditorComponent implements OnInit {

  drink: Drink = {
    id: null,
    name: null,
    price: null,
    volume: null,
  };
  drinks: Drink[] = [
    {
      id: 1,
      name: 'beer',
      price: 23,
      volume: '0.4L'
    },
    {
      id: 1,
      name: 'white wine',
      price: 23,
      volume: '0.2L'
    },
    {
      id: 1,
      name: 'red wine',
      price: 23,
      volume: '0.2L'
    },
    {
      id: 1,
      name: 'vodka',
      price: 23,
      volume: '0.3L'
    },
    {
      id: 1,
      name: 'cider',
      price: 23,
      volume: 'Bottle'
    }
  ];
  events: DrinkEvent[] = [
    {
      date: Date.now().toString(),
      name: 'vodka',
    }
  ];

  onBuy(drink: Drink): void {
    const event = {
      date: Date.now().toString(),
      name: drink.name,
    };
    this.events.push(event);
  }

  constructor() { }

  ngOnInit() {
  }

}

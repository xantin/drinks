import { Injectable } from '@angular/core';
import { Event, Drink, Stat } from './interfaces';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DrinkService {
  async getDrinks(): Promise<Drink[]> {
    return [
      { value: 20, id: 1, name: 'vodka', color: '#d0021b', likes: 23 },
      { value: 15, id: 1, name: 'beer', color: '#f5a623', likes: 23 },
      { value: 9, id: 1, name: 'white wine', color: '#f4e31c', likes: 23 },
      { value: 8, id: 1, name: 'red wine', color: '#7ed321', likes: 23 },
      { value: 11, id: 1, name: 'shoot', color: '#4582cb', likes: 23 },
    ].sort((v1: any, v2: any) => (v2.value - v1.value));
  }

  constructor() { }

  async getEvents(): Promise<any[]> {
    return [
      [moment('2018-08-07 08:00:00', 'YYYY-MM-DD hh:mm:ss').valueOf(), 'vodka2'],
      [moment('2018-08-07 08:01:00', 'YYYY-MM-DD hh:mm:ss').valueOf(), 'vodka'],
      [moment('2018-08-07 07:00:00', 'YYYY-MM-DD hh:mm:ss').valueOf(), 'beer'],
      [moment('2018-08-07 09:00:00', 'YYYY-MM-DD hh:mm:ss').valueOf(), 'vodka'],
      [moment('2018-08-07 06:00:00', 'YYYY-MM-DD hh:mm:ss').valueOf(), 'red wine'],
      [moment('2018-08-07 11:00:00', 'YYYY-MM-DD hh:mm:ss').valueOf(), 'red wine'],
    ].sort((a: any, b: any) => (b[0] - a[0]));
  }

  async getStats(): Promise<Stat[]> {
    const data = [
      { percent: 25, value: 1.5, title: 'avarage alcohol', subtitle: 'in blood of person' },
      { percent: 75, value: 129, title: 'total drinks', subtitle: 'used on event' },
      { percent: 0, value: 25585, title: 'total price', subtitle: 'for all drinks' },
      { percent: 25, value: 34, title: 'people counter', subtitle: 'people on event' },
    ];
    return data;
  }
}

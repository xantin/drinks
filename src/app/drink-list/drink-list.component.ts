import { Component, OnInit } from '@angular/core';
import { DrinkService } from '../drink.service';

@Component({
  selector: 'app-drink-list',
  templateUrl: './drink-list.component.html',
  styleUrls: ['./drink-list.component.css']
})
export class DrinkListComponent implements OnInit {
  headers;
  rows;
  constructor(private drinkService: DrinkService) { }

  async ngOnInit() {
    const data = await this.drinkService.getDrinks();
    this.headers = ['Drinks', '#', 'like'];
    this.rows = data;
  }

}
